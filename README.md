# Teste para Desenvolvedor na Fundação Casper Líbero


Gostaria de trabalhar conosco? Saiba o que esperamos de um desenvolvedor clicando [aqui](PERFIL.md).

Para ser um desenvolvedor na [Fundação Casper Líbero](http://fcl.com.br/) você deverá provar sua habilidade em resolver os problemas que iremos propor a seguir.

Você deverá escolher um dos problemas abaixo, efetuar a implementação de acordo com as premissas que estão explicadas mais abaixo e nos enviar o código para analise.

* [Cálculo de Tarifas Telefônicas](TESTE-2.md)
* [Desafio dos OVNIs](TESTE-3.md)

Após haver concluído a solução, basta nos enviar o link do repositório para *tpelizoni@fcl.com.br* ao qual, após analisarmos, iremos entrar em contato.