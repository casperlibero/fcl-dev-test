# Perfil do desenvolvedor que procuramos para a [FCL](http://fcl.com.br/)

* Autodidata
* Comprometido
* Auto gerenciável
* Flexível
* Saber trabalhar em equipe

----------------------------------------------------------------------------------------------------

# Requisitos Técnicos

## Desenvolvedor Backend

* Linux
* Conhecimento em Design de Código baseado em [SOLID](http://www.oodesign.com/design-principles.html)
* Git
* APIs REST (desenvolvimento e consumo)
* Conhecimento em banco de dados relacional
* Proficiência em Inglês (leitura e escrita)
* Estar ápto a desenvolver/manter sistemas com diferentes linguagens de programação (incluindo legados)

Dominar pelo menos uma das linguagens abaixo:

* PHP
* Python
* Ruby

----------------------------------------------------------------------------------------------------

## Desenvolvedor Frontend

* JavaScript
* Type Script
* CSS
* Bootstrap
* Git

### Diferenciais 

* Angular
* Ionic
