Desafio dos OVNIs
=================

Dizem que por detrás de cada cometa há um OVNI. Esses OVNIs tem por objetivo buscar bons desenvolvedores na Terra.

Infelizmente, só há espaço suficiente para levar um grupo de desenvolvedores por vez. Afim de selecioná-los, há um engenhoso esquema, da associação do nome do cometa ao nome do grupo, que possibilita a cada grupo saber se será levado ou não.

Os dois nomes, do grupo e do cometa, são convertidos em um número que representa o produto das letras do nome, onde "A" é 1 e "Z" é 26. Assim, o grupo "LARANJA" seria 12 * 1* 18 * 1 * 14 * 10 * 1 = 30240. 

Se o resto da divisão do número do grupo por 45 for igual ao resto da divisão do número do cometa por 45, então o grupo será levado.

Para os cometas e grupos abaixo, você deverá desenvolvedor um programa ao qual dirá o grupo que **não** será levado?

| Cometa   | Grupo    |
|:--------:|:--------:|
| HALLEY	 | AMARELO  |
| ENCKE	   | VERMELHO |
| WOLF     | PRETO   |
| KUSHIDA	 | AZUL     |
