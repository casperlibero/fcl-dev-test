Cálculo de Tarifas Telefônicas 
================

### Como o teste deverá ser feito?

Este teste tem por foco a vaga de programador Frontend. Seguindo os requisitos do [perfil desejado](https://bitbucket.org/casperlibero/fcl-dev-test/src/master/PERFIL.md), seu programa deverá ter:

* Uma interface gráfica utilizando SASS;
* A regra de negócio poderá ser feita utilizando algum framework de sua preferência como Angular, React, Vue.js, entre outros;
* A entrada de dados deverá ser feita pela interface gráfica, e seu resultado deverá ser exibido na mesma tela;

### O que será avaliado?

* A lógica empregada para a solução do problema;
* A estrutura do HTML e do SASS;
* A usabilidade da interface;
* Principios de interface do usuário: diagramação, proximidade, arquitetura de informação, contraste, cores;

# Regra de negócio

Uma empresa de telefonia especializada em chamadas de longa distância nacional, vai colocar um novo produto no mercado chamado FaleMuito.
 
Normalmente um cliente desta operadora pode fazer uma chamada de uma cidade para outra pagando uma tarifa fixa por minuto, com o preço sendo pré ­definido em uma lista com os códigos DDDs de origem e destino: 

| Origem        | Destino           | R$/minuto  |
|:-------------:|:-----------------:|:----------:|
| 11            | 16                | R$ 1,90    |
| 16            | 11                | R$ 2,90    |
| 11            | 17                | R$ 1,70    |
| 17            | 11                | R$ 2,70    |
| 11            | 18                | R$ 0,90    |
| 18            | 11                | R$ 1,90    |

| Origem        | Descrição         |
| ------------- |:-----------------:|
| 011 | São Paulo |
| 016 | Ribeirão Preto |
| 017 | Mirassol |
| 018 | Tupi Paulista |

Com o novo produto FaleMuito, o cliente adquire um plano e pode falar de graça  até um determinado tempo (em minutos) e só paga os minutos excedentes. Os minutos excedentes tem um acrescimo de 10% sobre a tarifa normal do minuto. Os planos são FaleMais 30 (30 minutos), FaleMais 60 (60 minutos) e FaleMais 120 (120 minutos). 

| Planos        | Tempo |
| ------------- |:--:|
| FaleMuito 30 | 30 |
| FaleMuito 60 | 60 |
| FaleMuito 120 | 120 |

A operadora preocupada com a transparência junto aos seus clientes, quer disponibilizar um serviço onde o cliente pode calcular o valor da ligação. Ali, o cliente pode escolher os códigos das cidades de origem e destino, o tempo da ligação em minutos e comparar os planos FaleMuito. O sistema deve mostrar os valores da ligação com os plano e sem o plano. 
O custo inicial de aquisição do plano deve ser desconsiderado para este  problema. 

Exemplo de valores
------------------

| Origem | Destino  | Tempo  | Plano         | Com Plano | Sem Plano |
|:------:|:--------:|:------:|:-------------:|:---------:|:---------:|
| 11     | 16       | 20     | FaleMuito 30  | R$ 0,00   | R$ 38,00  |
| 11     | 17       | 80     | FaleMuito 60  | R$ 37,40  | R$ 136,00 |
| 18     | 11       | 200    | FaleMuito 120 | R$ 167,20 | R$ 380,00 |
| 18     | 17       | 100    | FaleMuito 30  | -         | -         |

Funcionamento do Programa
---------------------------
Seu programa deverá receber os parâmetros e processar a saída conforme exemplo a seguir:

Entrada válida
--------------
```
origem: 18 destino: 11 tempo: 200
```

### Saída 

| Plano       | Valor     |
|:-----------:|:-----------:|
| FaleMuito 30 |  R$ 355,30 |
| FaleMuito 60 |  R$ 292,60 |
| FaleMuito 120 | R$ 167,20 |
| Normal |  R$ 380,00 |

Entrada inválida
-----------------
```
origem: 18 destino: 17 tempo: 200
```

### Saída

| Plano       | Valor     |
|:-----------:|:-----------:|
| FaleMuito 30 |  - |
| FaleMuito 60 |  - |
| FaleMuito 120 | - |
| Normal |  - |
